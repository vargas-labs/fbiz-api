# Dependencies 

To run this project locally, you will need .NET Core 1.0. 
You can download it from this URL: https://www.microsoft.com/net/download

#Running
- dotnet restore
- dotnet build
- dotnet run