﻿using Fbiz.Domain.Repositories.Catalogue.Category;
using Fbiz.Domain.Repositories.Catalogue.Product;
using Fbiz.Domain.Repositories.Catalogue.ProductComment;
using Fbiz.Filters.Log;
using Fbiz.Logger;
using Fbiz.Logger.Sql;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Fbiz.Dependency
{
    public static class InjectionResolverExtension
    {
        public static void AddInternalDependencies(this IServiceCollection services)
        {
            GetRegistraions()
                .ForEach(r => services.AddTransient(r.Service, r.Implementation));

            services.AddScoped<LoggerFilter>();
        }

        private static List<TypeRegistration> GetRegistraions()
        {
            var registrations = new List<TypeRegistration>();

            //TODO: Get all interfaces implementing IInjectionResolver and register the injection dynamically
            registrations.Add(new TypeRegistration(typeof(IProductRepository), typeof(ProductRepository)));
            registrations.Add(new TypeRegistration(typeof(ICategoryRepository), typeof(CategoryRepository)));
            registrations.Add(new TypeRegistration(typeof(IProductCommentRepository), typeof(ProductCommentRepository)));
            registrations.Add(new TypeRegistration(typeof(ILogger), typeof(SqlLogger)));

            return registrations;
        }

        private class TypeRegistration
        {
            public TypeRegistration(Type service, Type implementation)
            {
                Service = service;
                Implementation = implementation;
            }

            public Type Service { get; set; }

            public Type Implementation { get; set; }
        }
    }
}
