﻿namespace Fbiz.Dependency
{
    /// <summary>
    /// Markup class for auto register dependencies
    /// </summary>
    public interface IInjectionResolver { }
}
