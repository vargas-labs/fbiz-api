﻿using Fbiz.Domain.Contexts.Catalogue;
using Fbiz.Domain.Contexts.Log;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Fbiz.Dependency
{
    public static class ContextResolverExtension
    {
        public static void AddInternalContexts(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<CatalogueContext>(ops => ops.UseSqlServer(config.GetConnectionString("Catalogue")));
            services.AddDbContext<LogContext>(ops => ops.UseSqlServer(config.GetConnectionString("Catalogue")));
        }
    }
}
