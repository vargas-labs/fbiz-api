﻿using Fbiz.Domain.Entities.Catalogue;
using Fbiz.Domain.Repositories.Catalogue.Category;
using Fbiz.Domain.Repositories.Catalogue.Product;
using Fbiz.Filters.Log;
using Fbiz.Logger;
using Fbiz.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Fbiz.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ServiceFilter(typeof(LoggerFilter))]
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _repository;
        private readonly IProductRepository _productRepository;
        private readonly ILogger _logger;

        public CategoryController(
            ILogger logger,
            ICategoryRepository repository,
            IProductRepository productRepository)
        {
            _logger = logger;
            _repository = repository;
            _productRepository = productRepository;
        }
        
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var categories = await _repository.Get(_ => true);

                return Ok(categories.Select(c => new CategoryViewModel(c)));
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);

                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            try
            {
                var category = (await _repository.Get(c => c.Id == id))
                    .FirstOrDefault();

                if (category == null)
                    return NotFound();

                return Ok(new CategoryViewModel(category));
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);
                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }



        [HttpGet]
        [Route("{id}/products")]
        public async Task<IActionResult> GetProductsById([FromRoute] int id)
        {
            try
            {
                var products = await _productRepository.Get(p => p.CategoryId == id);

                if (!products.Any())
                    return NotFound();

                return Ok(products.Select(p => new ProductViewModel(p)));
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);
                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoryViewModel model)
        {
            try
            {
                var category = await _repository.Insert(new Category(model));

                return Created($"/api/category/{category.Id}", new CategoryViewModel(category));
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);
                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }
    }
}
