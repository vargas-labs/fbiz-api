﻿using Microsoft.AspNetCore.Mvc;

namespace Fbiz.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Redirect("/swagger/ui");
        }
    }
}
