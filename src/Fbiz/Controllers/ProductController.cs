﻿using Fbiz.Domain.Entities.Catalogue;
using Fbiz.Domain.Repositories.Catalogue.Product;
using Fbiz.ViewModel;
using Fbiz.ViewModel.Pagination;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Linq;
using Fbiz.Logger;
using Fbiz.Filters.Log;
using Fbiz.Domain.Repositories.Catalogue.ProductComment;
using Fbiz.ViewModel.Pagination.Product;

namespace Fbiz.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ServiceFilter(typeof(LoggerFilter))]
    public class ProductController : Controller
    {
        private readonly ILogger _logger;
        private readonly IProductRepository _repository;
        private readonly IProductCommentRepository _commentRepository;

        public ProductController(
            ILogger logger,
            IProductRepository repository,
            IProductCommentRepository commentRepository)
        {
            _logger = logger;
            _repository = repository;
            _commentRepository = commentRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ProductPaginationRequestViewModel pagination)
        {
            try
            {
                var products = await _repository.Get(new Pagination(pagination),
                    p => pagination.CategoryId > 0
                        ? (p.CategoryId == pagination.CategoryId)
                        : true);

                if (products == null || products.Data == null || !products.Data.Any())
                    return NotFound(products);

                return Ok(new PaginationResponseViewModel<ProductViewModel>
                {
                    Data = products.Data.Select(p => new ProductViewModel(p)),
                    Page = products.Page,
                    RowsPerPage = products.RowsPerPage,
                    TotalPages = products.TotalPages
                });
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);
                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            try
            {
                var product = (await _repository.Get(c => c.Id == id))
                    .FirstOrDefault();

                if (product == null)
                    return NotFound();

                return Ok(new ProductViewModel(product));
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);
                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        [Route("{id}/comments")]
        public async Task<IActionResult> GetCommentsByProductId([FromRoute] int id, [FromQuery] PaginationRequestViewModel pagination)
        {
            try
            {
                var producComments = await _commentRepository.Get(new Pagination(pagination), pc => pc.ProductId == id);

                if (producComments == null || producComments.Data == null || !producComments.Data.Any())
                    return NotFound(producComments);

                return Ok(new PaginationResponseViewModel<ProductCommentViewModel>
                {
                    Data = producComments.Data.Select(c => new ProductCommentViewModel(c)),
                    Page = producComments.Page,
                    RowsPerPage = producComments.RowsPerPage,
                    TotalPages = producComments.TotalPages
                });
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);
                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        [Route("{id}/comments")]
        public async Task<IActionResult> PostCommentsByProductId([FromRoute] int id, [FromBody] ProductCommentViewModel comment)
        {
            try
            {
                var product = await _repository.Get(c => c.Id == id);

                if (!product.Any())
                    return BadRequest();

                var productComment = await _commentRepository.Insert(new ProductComment(id, comment));

                return Created($"api/product/{id}/comments", new ProductCommentViewModel(productComment));
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);
                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductViewModel model)
        {
            try
            {
                var product = await _repository.Insert(new Product(model));

                return Created($"/api/product/{product.Id}", new ProductViewModel(product));
            }
            catch (Exception e)
            {
                await _logger.Write(LogLevel.Error, e);
                return StatusCode((int)HttpStatusCode.InternalServerError, e);
            }
        }
    }
}
