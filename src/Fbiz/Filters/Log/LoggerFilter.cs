﻿using Fbiz.Logger;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Fbiz.Filters.Log
{
    public class LoggerFilter : ActionFilterAttribute
    {
        private readonly ILogger _logger;

        public LoggerFilter(ILogger logger)
        {
            _logger = logger;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var request = context.HttpContext.Request;

            _logger.Write(LogLevel.Info, new { Request = new { context.ActionArguments, context.HttpContext.Request.Path } })
                .GetAwaiter()
                .GetResult();
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var response = context.HttpContext.Response;

            _logger.Write(LogLevel.Info, new { Response = new { context.Result, StatusCode = response.StatusCode } })
                .GetAwaiter()
                .GetResult();
        }
    }
}
