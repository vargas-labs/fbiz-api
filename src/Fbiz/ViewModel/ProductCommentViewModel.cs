﻿using Fbiz.Domain.Entities.Catalogue;
using System;
using System.ComponentModel;

namespace Fbiz.ViewModel
{
    public class ProductCommentViewModel
    {
        public ProductCommentViewModel()
        {
            RegisterDate = DateTime.Now;
            Active = true;
        }

        public ProductCommentViewModel(ProductComment comment)
        {
            Name = comment.Name;
            Title = comment.Title;
            Comment = comment.Comment;
            Id = comment.Id;
            RegisterDate = comment.RegisterDate;
            Active = comment.Active;
        }

        public long Id { get; set; }

        public bool Active { get; set; }
        
        public string Name { get; set; }

        public string Title { get; set; }

        public string Comment { get; set; }
        
        public DateTime RegisterDate { get; set; }
    }
}
