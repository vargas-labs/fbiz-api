﻿using Fbiz.Domain.Entities.Catalogue;
using System;

namespace Fbiz.ViewModel
{
    public class ProductViewModel
    {
        public ProductViewModel() { }

        public ProductViewModel(Product product)
        {
            Id = product.Id;
            Active = product.Active;
            RegisterDate = product.RegisterDate;
            Name = product.Name;
            Description = product.Description;
            CategoryId = product.CategoryId;
        }

        public int Id { get; set; }

        public bool Active { get; set; }

        public DateTime RegisterDate { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }
    }
}
