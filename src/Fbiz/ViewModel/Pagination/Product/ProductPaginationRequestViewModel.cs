﻿namespace Fbiz.ViewModel.Pagination.Product
{
    public class ProductPaginationRequestViewModel : PaginationRequestViewModel
    {
        public ProductPaginationRequestViewModel()
        {
            OrderBy = "name";
        }

        public int CategoryId { get; set; }
    }
}
