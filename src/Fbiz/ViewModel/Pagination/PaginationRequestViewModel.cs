﻿using System.ComponentModel;

namespace Fbiz.ViewModel.Pagination
{
    /// <summary>
    /// PaginationViewModel
    /// </summary>
    public class PaginationRequestViewModel
    {
        /// <summary>
        /// Page number
        /// </summary>
        [DefaultValue(1)]
        public int Page { get; set; }

        /// <summary>
        /// Rows per Page
        /// </summary>
        [DefaultValue(10)]
        public int RowsPerPage { get; set; }

        public string OrderBy { get; set; }
    }
}
