﻿using Fbiz.Domain.Entities.Catalogue;
using System;

namespace Fbiz.ViewModel
{
    public class CategoryViewModel
    {
        public CategoryViewModel() { }

        public CategoryViewModel(Category category)
        {
            Id = category.Id;
            Active = category.Active;
            RegisterDate = category.RegisterDate;
            Name = category.Name;
        }

        public int Id { get; set; }

        public bool Active { get; set; }

        public DateTime RegisterDate { get; set; }

        public string Name { get; set; }
    }
}
