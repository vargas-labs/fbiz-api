﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Fbiz.ViewModel.Pagination
{
    /// <summary>
    /// PaginationViewModel
    /// </summary>
    public class PaginationResult<T>
    {
        /// <summary>
        /// Page number
        /// </summary>
        [DefaultValue(1)]
        public int Page { get; set; }

        /// <summary>
        /// Rows per Page
        /// </summary>
        [DefaultValue(10)]
        public int RowsPerPage { get; set; }

        /// <summary>
        /// Total pages
        /// </summary>
        [DefaultValue(1)]
        public int TotalPages { get; set; }

        /// <summary>
        /// Pagination data
        /// </summary>
        public IEnumerable<T> Data { get; set; }
    }
}
