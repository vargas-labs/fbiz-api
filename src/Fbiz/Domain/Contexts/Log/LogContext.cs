﻿using Fbiz.Domain.Entities.Log;
using Microsoft.EntityFrameworkCore;

namespace Fbiz.Domain.Contexts.Log
{
    public class LogContext : DbContext
    {
        public LogContext(DbContextOptions<LogContext> options) : base(options) { }

        public DbSet<ApiLog> ApiLog { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApiLog>(builder =>
            {
                builder.HasKey(pk => pk.Id);
                builder.Property(p => p.Level).IsRequired();
                builder.Property(p => p.Log).IsRequired();
            });
        }
    }
}
