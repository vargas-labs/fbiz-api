﻿using Fbiz.Domain.Entities.Catalogue;
using Fbiz.Domain.Entities.Log;
using Microsoft.EntityFrameworkCore;
namespace Fbiz.Domain.Contexts.Catalogue
{
    public class CatalogueContext : DbContext
    {
        public CatalogueContext(DbContextOptions<CatalogueContext> options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Product> Product { get; set; }

        public DbSet<Category> Category { get; set; } 

        public DbSet<ProductComment> ProductComment { get; set; }

        /// <summary>
        /// TODO: This entity should be in another database (bounded context)
        /// The current Sql Server plan, allow just one database.
        /// </summary>
        public DbSet<ApiLog> ApiLog { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>(builder =>
            {
                builder.HasKey(pk => pk.Id);
                builder.Property(p => p.Active).IsRequired();
                builder.Property(p => p.RegisterDate).IsRequired();

                builder.Property(p => p.Name)
                    .HasMaxLength(200)
                    .IsRequired();
                builder.HasOne(p => p.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(fk => fk.CategoryId)
                    .HasConstraintName("FK_Product_Category");
            });

            modelBuilder.Entity<Category>(builder =>
            {
                builder.HasKey(pk => pk.Id);
                builder.Property(p => p.Active).IsRequired();
                builder.Property(p => p.RegisterDate).IsRequired();

                builder.Property(p => p.Name)
                    .HasMaxLength(200)
                    .IsRequired();
            });
            
            modelBuilder.Entity<ProductComment>(builder =>
            {
                builder.HasKey(pk => pk.Id);
                builder.Property(p => p.Active).IsRequired();
                builder.Property(p => p.RegisterDate).IsRequired();

                builder.Property(p => p.Name)
                    .HasMaxLength(200)
                    .IsRequired();
                builder.Property(p => p.Title)
                    .HasMaxLength(200)
                    .IsRequired();
                builder.Property(p => p.Comment)
                    .IsRequired();

                builder.HasOne(p => p.Product)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(fk => fk.ProductId)
                    .HasConstraintName("FK_Product_ProductComment");
            });

            modelBuilder.Entity<ApiLog>(builder =>
            {
                builder.HasKey(pk => pk.Id);
                builder.Property(p => p.Level).IsRequired();
                builder.Property(p => p.Log).IsRequired();
            });
        }
    }
}
