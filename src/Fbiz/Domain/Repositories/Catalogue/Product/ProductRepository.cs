﻿using Fbiz.Domain.Contexts.Catalogue;

namespace Fbiz.Domain.Repositories.Catalogue.Product
{
    public class ProductRepository : RepositoryBase<Entities.Catalogue.Product, int>, IProductRepository
    {
        public ProductRepository(CatalogueContext context) : base(context) { }
    }
}
