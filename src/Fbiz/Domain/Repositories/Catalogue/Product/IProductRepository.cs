﻿namespace Fbiz.Domain.Repositories.Catalogue.Product
{
    public interface IProductRepository : IRepository<Entities.Catalogue.Product>
    {
    }
}
