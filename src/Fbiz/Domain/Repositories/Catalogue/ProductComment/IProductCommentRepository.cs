﻿namespace Fbiz.Domain.Repositories.Catalogue.ProductComment
{
    public interface IProductCommentRepository : IRepository<Entities.Catalogue.ProductComment>
    {
    }
}
