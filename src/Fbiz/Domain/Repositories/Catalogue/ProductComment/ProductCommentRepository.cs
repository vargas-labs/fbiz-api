﻿using Fbiz.Domain.Contexts.Catalogue;

namespace Fbiz.Domain.Repositories.Catalogue.ProductComment
{
    public class ProductCommentRepository : RepositoryBase<Entities.Catalogue.ProductComment, long>, IProductCommentRepository
    {
        public ProductCommentRepository(CatalogueContext context) : base(context) { }
    }
}
