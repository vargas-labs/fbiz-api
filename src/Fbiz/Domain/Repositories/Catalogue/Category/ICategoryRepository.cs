﻿namespace Fbiz.Domain.Repositories.Catalogue.Category
{
    public interface ICategoryRepository : IRepository<Entities.Catalogue.Category>
    {
    }
}
