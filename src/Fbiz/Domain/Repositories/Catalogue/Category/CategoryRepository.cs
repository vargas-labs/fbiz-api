﻿using Fbiz.Domain.Contexts.Catalogue;

namespace Fbiz.Domain.Repositories.Catalogue.Category
{
    public class CategoryRepository : RepositoryBase<Entities.Catalogue.Category, int>, ICategoryRepository
    {
        public CategoryRepository(CatalogueContext context) : base(context) { }
    }
}
