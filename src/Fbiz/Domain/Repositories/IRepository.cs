﻿using Fbiz.ViewModel.Pagination;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Fbiz.Domain.Repositories
{
    public interface IRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> predicate);

        Task<PaginationResult<TEntity>> Get(Pagination pagination, Expression<Func<TEntity, bool>> predicate);

        Task<bool> Update(TEntity entry);

        Task<TEntity> Insert(TEntity entry);

        Task<bool> Delete(TEntity entry);
    }
}
