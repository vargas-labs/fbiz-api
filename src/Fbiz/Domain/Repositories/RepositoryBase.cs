﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Fbiz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Fbiz.ViewModel.Pagination;
using System.Reflection;

namespace Fbiz.Domain.Repositories
{
    public class RepositoryBase<TEntity, TPrimaryKey> : IRepository<TEntity>
        where TEntity : EntityBase<TPrimaryKey>
        where TPrimaryKey : struct
    {
        protected readonly DbContext _context;

        public RepositoryBase(DbContext context)
        {
            _context = context;
        }

        public virtual async Task<bool> Delete(TEntity entry)
        {
            var data = await _context.Set<TEntity>()
                .FirstOrDefaultAsync(e => EqualityComparer<TPrimaryKey>.Default.Equals(e.Id, entry.Id));

            if (data == null)
                return false;

            _context.Set<TEntity>()
                .Remove(data);

            await _context.SaveChangesAsync();

            return true;
        }

        public virtual async Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>()
                .Where(predicate)
                .ToArrayAsync();
        }

        public virtual async Task<PaginationResult<TEntity>> Get(Pagination pagination, Expression<Func<TEntity, bool>> predicate)
        {
            var data = _context.Set<TEntity>()
                .Where(predicate)
                .OrderBy(x => GetOrderByProperty(pagination.OrderBy, x));

            var skip = (pagination.Page - 1) * pagination.RowsPerPage;
            var take = pagination.RowsPerPage;
            var rows = await data
                .Skip(skip)
                .Take(take)
                .ToArrayAsync();

            var totalPages = data.Count() / pagination.RowsPerPage;

            if (data.Count() % pagination.RowsPerPage > 0)
            {
                totalPages++;
            }

            return new PaginationResult<TEntity>
            {
                Data = rows,
                Page = pagination.Page,
                RowsPerPage = pagination.RowsPerPage,
                TotalPages = totalPages
            };
        }

        public virtual async Task<TEntity> Insert(TEntity entry)
        {
            _context.Set<TEntity>()
                .Add(entry);

            await _context.SaveChangesAsync();

            return entry;
        }

        public virtual async Task<bool> Update(TEntity entry)
        {
            var row = _context.Set<TEntity>()
                .FirstOrDefaultAsync(e => EqualityComparer<TPrimaryKey>.Default.Equals(e.Id, entry.Id));

            if (row != null)
            {
                _context.Attach(entry)
                    .State = EntityState.Modified;

                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }

        private object GetOrderByProperty(string propertyName, TEntity entity)
        {
            try
            {
                var property = typeof(TEntity).GetProperties()
                    .FirstOrDefault(p => string.Equals(p.Name, propertyName, StringComparison.CurrentCultureIgnoreCase));
                return property.GetValue(entity);
            }
            catch (Exception)
            {
                var property = typeof(TEntity).GetProperty("Id");
                return property.GetValue(entity);
            }
        }
    }
}
