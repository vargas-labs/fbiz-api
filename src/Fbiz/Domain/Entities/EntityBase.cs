﻿using System;

namespace Fbiz.Domain.Entities
{
    public class EntityBase<TPrimaryKey>
        where TPrimaryKey : struct
    {
        public EntityBase() { }

        public TPrimaryKey Id { get; set; }
    }
}
