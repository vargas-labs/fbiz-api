﻿using Fbiz.Domain.Entities.Catalogue.Base;
using Fbiz.ViewModel;
using System;

namespace Fbiz.Domain.Entities.Catalogue
{
    public class ProductComment : CatalogueEntityBase<long>
    {
        public ProductComment() { }

        public ProductComment(int productId, ProductCommentViewModel model)
        {
            Name = model.Name;
            Title = model.Title;
            Comment = model.Comment;
            ProductId = productId;
            Id = model.Id;
            RegisterDate = DateTime.Now;
            Active = model.Active;
        }

        public string Name { get; set; }

        public string Title { get; set; }

        public string Comment { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}
