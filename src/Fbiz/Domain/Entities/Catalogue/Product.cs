﻿using Fbiz.Domain.Entities.Catalogue.Base;
using Fbiz.ViewModel;
using System;
using System.Collections.Generic;

namespace Fbiz.Domain.Entities.Catalogue
{
    public class Product : CatalogueEntityBase<int>
    {
        public Product() { }

        public Product(ProductViewModel product)
        {
            Id = product.Id;
            Active = product.Active;
            RegisterDate = DateTime.Now;
            Name = product.Name;
            Description = product.Description;
            CategoryId = product.CategoryId;
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual  List<ProductComment> Comments { get; set; }
    }
}
