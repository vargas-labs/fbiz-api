﻿using System;

namespace Fbiz.Domain.Entities.Catalogue.Base
{
    public class CatalogueEntityBase<TPrimaryKey> : EntityBase<TPrimaryKey>
        where TPrimaryKey : struct
    {
        public bool Active { get; set; }

        public DateTime RegisterDate { get; set; }
    }
}
