﻿using Fbiz.Domain.Entities.Catalogue.Base;
using Fbiz.ViewModel;
using System;
using System.Collections.Generic;

namespace Fbiz.Domain.Entities.Catalogue
{
    public class Category : CatalogueEntityBase<int>
    {
        public Category() { }

        public Category(CategoryViewModel category)
        {
            Id = category.Id;
            Active = category.Active;
            RegisterDate = DateTime.Now;
            Name = category.Name;
        }

        public string Name { get; set; }

        public virtual List<Product> Products { get; set; }
    }
}
