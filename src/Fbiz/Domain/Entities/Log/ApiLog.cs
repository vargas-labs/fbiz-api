﻿using Fbiz.Logger;
using System;

namespace Fbiz.Domain.Entities.Log
{
    public class ApiLog : EntityBase<Guid>
    {
        public LogLevel Level { get; set; }

        public string Log { get; set; }
    }
}
