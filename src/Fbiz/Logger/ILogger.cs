﻿using System.Threading.Tasks;

namespace Fbiz.Logger
{
    public interface ILogger
    {
        Task Write<T>(LogLevel level, T log);
    }
}
