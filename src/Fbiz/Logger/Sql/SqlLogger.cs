﻿using Fbiz.Domain.Contexts.Catalogue;
using Fbiz.Domain.Entities.Log;
using Fbiz.Domain.Repositories;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Fbiz.Logger.Sql
{
    public class SqlLogger : RepositoryBase<ApiLog, Guid>, ILogger
    {
        public SqlLogger(CatalogueContext context) : base(context) { }

        public async Task Write<T>(LogLevel level, T log)
        {
            try
            {
                var json = JsonConvert.SerializeObject(log);

                await Insert(new ApiLog
                {
                    Level = level,
                    Log = json
                });
            }
            catch (Exception) { }
        }
    }
}
