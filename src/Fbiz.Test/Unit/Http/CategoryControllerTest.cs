﻿using Fbiz.Controllers;
using Fbiz.Domain.Entities.Catalogue;
using Fbiz.Domain.Repositories.Catalogue.Category;
using Fbiz.Domain.Repositories.Catalogue.Product;
using Fbiz.Test.Common;
using Fbiz.Test.Http.Repository.Catalogue;
using Fbiz.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Xunit;

namespace Fbiz.Test.Unit.Http
{
    public class CategoryControllerTest
    {
        private readonly CategoryController _controller;
        private readonly ICategoryRepository _repository;
        private readonly IProductRepository _productRepository;

        public CategoryControllerTest()
        {
            _repository = new CategoryRepositoryMock();
            _productRepository = new ProductRepositoryMock();
            _controller = new CategoryController(TestLogger.GetLogger(), _repository, _productRepository);
        }

        [Fact]
        public void GetMustReturnOk()
        {
            var action = _controller.Get()
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<OkObjectResult>(action);

            Assert.True(result.StatusCode == (int)HttpStatusCode.OK);
        }

        [Fact]
        public void GetCategoryMustReturnOk()
        {
            var entry =_repository.Insert(new Category())
                .GetAwaiter()
                .GetResult();

            var action = _controller.Get(entry.Id)
                .GetAwaiter()
                .GetResult();
            
            var result = Assert.IsType<OkObjectResult>(action);
            Assert.True(result.StatusCode == (int)HttpStatusCode.OK);
        }

        [Fact]
        public void GetCategoryMustReturnNotFound()
        {
            var action = _controller.Get(new Random().Next())
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<NotFoundResult>(action);
            Assert.True(result.StatusCode == (int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void GetProductsByCategoryIdMustReturnOk()
        {
            var category = _repository.Insert(new Category()).GetAwaiter().GetResult();
            var product = _productRepository.Insert(new Product { CategoryId = category.Id }).GetAwaiter().GetResult();

            var action = _controller.GetProductsById(category.Id)
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<OkObjectResult>(action);
            var products = (IEnumerable<ProductViewModel>)result.Value;

            Assert.True(result.StatusCode == (int)HttpStatusCode.OK);
            Assert.True(products.Any(p => p.Id == product.Id));
        }

        [Fact]
        public void GetProductsByCategoryIdMustReturnNotFound()
        {
            var category = _repository.Insert(new Category()).GetAwaiter().GetResult();

            var action = _controller.GetProductsById(category.Id)
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<NotFoundResult>(action);
            Assert.True(result.StatusCode == (int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void PostMustReturnCreated()
        {
            var categoryViewModel = new CategoryViewModel();

            var action = _controller.Post(categoryViewModel)
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<CreatedResult>(action);
            categoryViewModel = Assert.IsType<CategoryViewModel>(result.Value);

            Assert.True(result.StatusCode == (int)HttpStatusCode.Created);
            Assert.True(result.Location.Contains($"{categoryViewModel.Id}"));
        }
    }
}
