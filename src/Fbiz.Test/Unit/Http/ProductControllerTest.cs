﻿using Fbiz.Controllers;
using Fbiz.Domain.Entities.Catalogue;
using Fbiz.Domain.Repositories.Catalogue.Product;
using Fbiz.Domain.Repositories.Catalogue.ProductComment;
using Fbiz.Test.Common;
using Fbiz.Test.Http.Repository.Catalogue;
using Fbiz.ViewModel;
using Fbiz.ViewModel.Pagination;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using Xunit;

namespace Fbiz.Test.Unit.Http
{
    public class ProductControllerTest
    {
        private readonly ProductController _controller;
        private readonly IProductRepository _productRepository;
        private readonly IProductCommentRepository _productCommentRepository;

        public ProductControllerTest()
        {
            _productRepository = new ProductRepositoryMock();
            _productCommentRepository = new ProductCommentRepositoryMock();
            _controller = new ProductController(TestLogger.GetLogger(), _productRepository, _productCommentRepository);
        }

        [Fact]
        public void GetMustReturnOk()
        {
            //var action = _controller.Get()
            //    .GetAwaiter()
            //    .GetResult();

            //var result = Assert.IsType<OkObjectResult>(action);

            //Assert.True(result.StatusCode == (int)HttpStatusCode.OK);
        }

        [Fact]
        public void GetProductMustReturnOk()
        {
            var entry = _productRepository.Insert(new Product())
                .GetAwaiter()
                .GetResult();

            var action = _controller.Get(entry.Id)
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<OkObjectResult>(action);
            Assert.True(result.StatusCode == (int)HttpStatusCode.OK);
        }

        [Fact]
        public void GetProductMustReturnNotFound()
        {
            var action = _controller.Get(new Random().Next())
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<NotFoundResult>(action);
            Assert.True(result.StatusCode == (int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void PostMustReturnCreated()
        {
            var productViewModel = new ProductViewModel();

            var action = _controller.Post(productViewModel)
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<CreatedResult>(action);
            productViewModel = Assert.IsType<ProductViewModel>(result.Value);

            Assert.True(result.StatusCode == (int)HttpStatusCode.Created);
            Assert.True(result.Location.Contains($"{productViewModel.Id}"));
        }

        [Fact]
        public void GetPaginationMustReturnOk()
        {
            var fakeProductId = 1;

            SetupMockItems(100, fakeProductId);

            var action = _controller.GetCommentsByProductId(fakeProductId, new PaginationRequestViewModel
            {
                Page = 5,
                RowsPerPage = 10
            }).GetAwaiter().GetResult();

            var result = Assert.IsType<OkObjectResult>(action);
            var pagination = Assert.IsType<PaginationResponseViewModel<ProductCommentViewModel>>(result.Value);

            Assert.True(result.StatusCode == (int)HttpStatusCode.OK);
            Assert.True(pagination.Page == 5);
            Assert.True(pagination.Data.Count() == 10);
            Assert.True(pagination.TotalPages == 10);
        }

        [Fact]
        public void GetPaginationMustReturnNotFound()
        {
            var fakeProductId = 1;

            var action = _controller.GetCommentsByProductId(fakeProductId, new PaginationRequestViewModel
            {
                Page = 5,
                RowsPerPage = 10
            }).GetAwaiter().GetResult();

            var result = Assert.IsType<NotFoundObjectResult>(action);
            Assert.True(result.StatusCode == (int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void PostProductCommentMustReturnCreated()
        {
            var product = _productRepository.Insert(new Product())
                .GetAwaiter()
                .GetResult();

            var commentViewModel = new ProductCommentViewModel();

            var action = _controller.PostCommentsByProductId(product.Id, commentViewModel)
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<CreatedResult>(action);
            commentViewModel = Assert.IsType<ProductCommentViewModel>(result.Value);

            Assert.True(result.StatusCode == (int)HttpStatusCode.Created);
            Assert.True(result.Location.Contains($"{product.Id}"));
        }

        [Fact]
        public void PostMustReturnBadRequest()
        {
            var commentViewModel = new ProductCommentViewModel();

            var action = _controller.PostCommentsByProductId(new Random().Next(), commentViewModel)
                .GetAwaiter()
                .GetResult();

            var result = Assert.IsType<BadRequestResult>(action);
            Assert.True(result.StatusCode == (int)HttpStatusCode.BadRequest);
        }

        private void SetupMockItems(int amount, int productId)
        {
            for (int i = 0; i < amount; i++)
            {
                _productCommentRepository.Insert(new ProductComment()
                {
                    ProductId = productId
                });
            }
        }
    }
}
