﻿using Fbiz.Domain.Entities.Catalogue;
using Fbiz.Domain.Repositories.Catalogue.Product;
using Fbiz.Test.Common;

namespace Fbiz.Test.Http.Repository.Catalogue
{
    public class ProductRepositoryMock : RepositoryBaseMock<Product, int>, IProductRepository { }
}
