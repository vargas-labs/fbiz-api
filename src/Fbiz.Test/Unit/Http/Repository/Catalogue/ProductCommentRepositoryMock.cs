﻿using Fbiz.Domain.Entities.Catalogue;
using Fbiz.Domain.Repositories.Catalogue.ProductComment;
using Fbiz.Test.Common;

namespace Fbiz.Test.Http.Repository.Catalogue
{
    public class ProductCommentRepositoryMock : RepositoryBaseMock<ProductComment, long>, IProductCommentRepository { }
}
