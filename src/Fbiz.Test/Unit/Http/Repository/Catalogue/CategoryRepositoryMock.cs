﻿using Fbiz.Domain.Repositories.Catalogue.Category;
using Fbiz.Domain.Entities.Catalogue;
using Fbiz.Test.Common;

namespace Fbiz.Test.Http.Repository.Catalogue
{
    public class CategoryRepositoryMock : RepositoryBaseMock<Category, int>, ICategoryRepository { }
}
