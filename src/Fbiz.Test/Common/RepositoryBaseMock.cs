﻿using Fbiz.Domain.Repositories;
using System;
using System.Collections.Generic;
using Fbiz.ViewModel.Pagination;
using System.Linq.Expressions;
using Fbiz.Domain.Entities;
using System.Threading.Tasks;
using System.Linq;

namespace Fbiz.Test.Common
{
    public class RepositoryBaseMock<TEntity, TPrimaryKey> : IRepository<TEntity>
        where TEntity : EntityBase<TPrimaryKey>
        where TPrimaryKey : struct
    {
        private List<TEntity> _current;

        public RepositoryBaseMock()
        {
            _current = new List<TEntity>();
        }

        public Task<bool> Delete(TEntity entry)
        {
            try
            {
                _current.Remove(entry);

                return Task.FromResult(true);
            }
            catch
            {
                return Task.FromResult(false);
            }
        }

        public Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.FromResult(_current.Where(predicate.Compile()));
        }

        public Task<PaginationResult<TEntity>> Get(Pagination pagination, Expression<Func<TEntity, bool>> predicate)
        {
            var data = _current.Where(predicate.Compile());

            var skip = (pagination.Page - 1) * pagination.RowsPerPage;
            var take = pagination.RowsPerPage;
            var rows = data
                .Skip(skip)
                .Take(take);

            var totalPages = data.Count() / pagination.RowsPerPage;

            if (data.Count() % pagination.RowsPerPage > 0)
            {
                totalPages++;
            }

            return Task.FromResult(new PaginationResult<TEntity>
            {
                Data = rows,
                Page = pagination.Page,
                RowsPerPage = pagination.RowsPerPage,
                TotalPages = totalPages
            });
        }

        public Task<TEntity> Insert(TEntity entry)
        {
            entry.Id = (TPrimaryKey)Convert.ChangeType(new Random().Next(), typeof(TPrimaryKey));

            _current.Add(entry);

            return Task.FromResult(entry);
        }

        public Task<bool> Update(TEntity entry)
        {
            var index = _current.IndexOf(entry);

            if (index >= 0)
            {
                _current.RemoveAt(index);
                _current.Add(entry);

                return Task.FromResult(true);
            }

            return Task.FromResult(false);
        }
    }
}
