﻿using Fbiz.Logger;
using System.Threading.Tasks;

namespace Fbiz.Test.Common
{
    public class TestLogger
    {
        private static BypassLogger _logger;

        public static ILogger GetLogger()
        {
            if (_logger == null)
                _logger = new BypassLogger();

            return _logger;
        }
    }

    public class BypassLogger : ILogger
    {
        public async Task Write<T>(LogLevel level, T log)
        {
            await Task.Run(() => { });
        }
    }
}
